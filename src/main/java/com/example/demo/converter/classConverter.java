package com.example.demo.converter;

import com.example.demo.dto.classDto;
import com.example.demo.models.classEntity;
import org.springframework.stereotype.Component;

@Component
public class classConverter {
    public classEntity toEntity(classDto dto){
        classEntity entity = new classEntity();
        entity.setId(dto.getId());
        entity.setTenLop(dto.getTenLop());
        return entity;
    }

    public classDto toDto(classEntity entity){
        classDto dto = new classDto();
        dto.setId(entity.getId());
        dto.setTenLop(entity.getTenLop());
        return dto;
    }

    public classEntity toEntity(classDto dto, classEntity entity){
        entity.setId(dto.getId());
        entity.setTenLop(dto.getTenLop());
        return entity;
    }
}
