package com.example.demo.CustomRepo;

import com.example.demo.dto.studentDto;
import com.example.demo.models.studentEntity;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface studentCustomRepo {
    List<studentEntity> getById(Integer studentId);

    List<studentEntity> getByEmpId(Integer studentId);

    Integer getNumberOfItemsByRequest(studentDto studentDto);

    List<studentDto> search(studentDto studentDto, Pageable pageable);
}
