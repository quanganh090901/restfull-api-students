package com.example.demo.CustomRepo;

import com.example.demo.dto.classDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface classCustomRepo {
     List<classDto> search(classDto departmentDto, Pageable pageable);

     Integer getNumberOfItemsByRequest(classDto departmentDto);

    void delete(Integer id);
}
