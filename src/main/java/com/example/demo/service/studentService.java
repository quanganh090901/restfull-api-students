package com.example.demo.service;

import com.example.demo.dto.classDto;
import com.example.demo.dto.classMapDto;
import com.example.demo.dto.studentDto;
import com.example.demo.dto.studentMapDto;
import org.springframework.data.domain.Pageable;

public interface studentService {
    studentDto save(studentDto studentDto);

    void deleteById(int studentId);

//    studentDto update(studentDto studentDto);
    studentMapDto search(studentDto studentDto, Pageable pageable);

}
