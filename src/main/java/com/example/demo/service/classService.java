package com.example.demo.service;

import com.example.demo.dto.classDto;
import com.example.demo.dto.classMapDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface classService {

    List<classDto> getAll();
    classDto save(classDto classDto);

    void deleteById(int classId);

    classMapDto search(classDto classDto, Pageable pageable);
}
