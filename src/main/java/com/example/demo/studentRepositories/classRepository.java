package com.example.demo.studentRepositories;

import com.example.demo.models.classEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface classRepository extends JpaRepository<classEntity, Integer> {
}
