package com.example.demo.studentRepositories;

import com.example.demo.models.studentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface studentRepository extends JpaRepository<studentEntity, Integer> {

}
