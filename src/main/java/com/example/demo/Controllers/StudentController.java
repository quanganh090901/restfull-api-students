package com.example.demo.Controllers;

import com.example.demo.dto.classDto;
import com.example.demo.dto.studentDto;
import com.example.demo.dto.studentMapDto;
import com.example.demo.models.studentEntity;
import com.example.demo.service.classService;
import com.example.demo.service.studentService;
import com.example.demo.studentRepositories.classRepository;
import com.example.demo.studentRepositories.studentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Optional;


@CrossOrigin
@RestController
public class StudentController {
    @Autowired
    studentService studentService;

    @Autowired
    classService classService;
    @Autowired
    studentRepository studentRepository;
    @Autowired
    classRepository classRepository;

    @GetMapping(value = "/students")
    public List<studentEntity> getAllNotes() {
        return studentRepository.findAll();
    }
    @PostMapping(value = "/students")
    public studentDto createStudent(@RequestBody studentDto model){
        return studentService.save(model);
    }

    @PutMapping(value = "students/{studentId}")
    public studentDto updateStudent(@RequestBody studentDto model, @PathVariable("studentId") int studentId){
        model.setId(studentId);
        return studentService.save(model);
    }

    @DeleteMapping(value = "/students/{studentId}")
    public void deleteStudent(@PathVariable("studentId")int studentId){
        studentService.deleteById(studentId);
    }

    @RequestMapping(value = "/students/{studentId}",method = RequestMethod.GET)
    public Optional<studentEntity> doView(@PathVariable("studentId") int studentId){
        return studentRepository.findById(studentId);
    }


//    @RequestMapping( value = "/student")
//    public ModelAndView doHome(@ModelAttribute("studentModel") studentDto studentDto, Pageable pageable) {
//        ModelAndView mv = new ModelAndView("Student/index");
//        studentMapDto result = studentService.search(studentDto,pageable);
//        List<classDto> depart = classService.getAll();
//        mv.addObject("student", result.getDtoList());
//        mv.addObject("totalItems",result.getNumberOfItems());
//        mv.addObject("class", depart);
//
//        return mv;
//    }
//
//    @RequestMapping(value = "/Employee/create")
//    public ModelAndView doCreate(){
//        ModelAndView mv = new ModelAndView("Employee/add");
//        return mv;
//    }
//
//    @RequestMapping(value = "/Employee/view/{empId}",method = RequestMethod.GET)
//    public ModelAndView doView(@PathVariable("empId") int empId) {
//        ModelAndView mv = new ModelAndView("Employee/view");
//        Optional<EmployeeDto> emp = employeeService.findById(empId);
//        if (emp.isPresent()){
//            mv.addObject("employee", emp.get());
//        }else{
//            System.out.println("nothing id here");
//        }
//        return mv;
//    }
//
//    @RequestMapping(value ="/Employee/save",method = RequestMethod.POST)
//    public ModelAndView doSave(@RequestParam("emp_name") String emp_name,
//                               @RequestParam("age")String age, @RequestParam("sex") String sex,
//                               @RequestParam("birthday") String birthday, @RequestParam("departId") String departId) throws ParseException {
//        ModelAndView mv = new ModelAndView("redirect:/emp");
//
//        Employee employee = new Employee();
//        employee.setEmpName(emp_name);
//        employee.setAge(Integer.parseInt(age));
//        employee.setSex(Integer.parseInt(sex));
//        SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");
//        employee.setBirthday(frm.parse(birthday));
//        employee.setDepartId(Integer.parseInt(departId));
//        employeeRepo.save(employee);
//
//
//        return mv;
//    }
//
//    @RequestMapping(value = "/Employee/edit/{empId}", method = RequestMethod.GET)
//    public ModelAndView showUpdate(@PathVariable("empId") int empId){
//        ModelAndView mv = new ModelAndView("Employee/edit");
//        List<DepartmentDto> depart = departmentService.getAll();
//        mv.addObject("department", depart);
//        Optional<EmployeeDto> emp = employeeService.findById(empId);
//        mv.addObject("employee", emp.get());
//        return mv;
//    }
//
//    @RequestMapping(value ="/Employee/update",method = RequestMethod.POST)
//    public ModelAndView doUpdate(@RequestParam("emp_id") String emp_id,@RequestParam("emp_name") String emp_name,
//                                 @RequestParam("age")String age, @RequestParam("sex") String sex,
//                                 @RequestParam("birthday") String birthday, @RequestParam("departId") String departId) throws ParseException {
//        ModelAndView mv = new ModelAndView("redirect:/emp");
//        Employee employee = new Employee();
//        employee.setEmpId(Integer.parseInt(emp_id));
//        employee.setEmpName(emp_name);
//        employee.setAge(Integer.parseInt(age));
//        employee.setSex(Integer.parseInt(sex));
//        SimpleDateFormat frm = new SimpleDateFormat("yyyy-MM-dd");
//        employee.setBirthday(frm.parse(birthday));
//        employee.setDepartId(Integer.parseInt(departId));
//        employeeRepo.save(employee);
//        return mv;
//
//    }
//
//
//    @RequestMapping(value = "/Employee/delete/{empId}", method = RequestMethod.GET)
//    public ModelAndView doDelete(@PathVariable("empId") int empId){
//        ModelAndView mv = new ModelAndView("redirect:/emp");
//        employeeService.deleteById(empId);
//        return mv;
//    }
}

