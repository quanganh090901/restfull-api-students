<!-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page isELIgnored="false" %> -->
<!DOCTYPE html>
<html>

<head>
<link href="/webjars/bootstrap/3.3.6/css/bootstrap.min.css"
 rel="stylesheet">
</head>

<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

    a {
        color: #ffffff;
    }

    .button span {
        padding: 10px 30px;
        border-radius: 5px;
        width: 15px !important;
        height: 18px;
    }
     #pagination {
          display: flex;
          display: -webkit-flex; /* Safari 8 */
          flex-wrap: wrap;
          -webkit-flex-wrap: wrap; /* Safari 8 */
          justify-content: center;
         -webkit-justify-content: center;
     }
</style>
<body>
<h1>${err}</h1>
    <div class="container">
        <h2 style="margin-top:20px;padding-bottom: 25px;">Lớp Học Demo</h2>
        <div class="alert alert-info" role="alert">
            ${msgDel}
        </div>
        <div class="col-md-12">
            <div class="col-md-3">
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                    data-target="#myModal">Create</button>
            </div>
            <div class="col-md-9">
                <!-- Search form -->
                <form method="get" action="/" name="classModel">

                    <input class="form-control" type="text" placeholder="Search" aria-label="Search" id="tenLop"
                        name="tenLop" style="width: 70%;float:left;">
                    <button type="submit" class="btn btn-primary">Search</button>
                    <h1>${err}</h1>
                </form>
            </div>
        </div>
        <!-- table -->
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>MaLop</th>
                    <th>TenLop</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                <c:forEach var="list" items="${class}">
                    <tr class="contentPage">
                                <td>${list.id}</td>
                                <td>${list.tenLop}</td>
                            <td style="width:347px;">
                                <div class="button">
                                <span class="span label-default"><a href="/view/${list.id}">Show Detail</a></span>
                                <button type="button" class="btn btn-info btn-lg" data-toggle="modal"
                                    data-target="#update${list.id}" style="padding: 4px 16px;">Edit</button>
                                <span class="span label-danger"><a onclick="return myFunction()"
                                        href="/delete/${list.id}">Delete</a></span>
                                </div>
                            </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

<ul id="pagination">


</ul>


<!-- end table -->

        <!-- modal -->
    <c:forEach var="list" items="${class}">
        <div id="update${list.id}" class="modal fade" role="dialog">
            <div class="modal-dialog" style="margin-left: 404px;">
                <!-- Modal content-->
                <div class="modal-content" style="height: 257px;">
        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Class</h4>
        </div>
          <div class="modal-body">
              <div class="container">
                 <form class="form-horizontal" method="post" action="/edit/save">
                     <div class="form-group">
                          <label class="control-label col-sm-2" for="id">MaLop</label>
                             <div class="col-sm-10">
                                  <input readonly style="width:350px; type=" text" class="form-control"
                                   id="id" name="id" value="${list.id}">
                              </div>
                     </div>

                     <div class="form-group">
                             <label class="control-label col-sm-2" for="tenLop">TenLop</label>
                        <div class="col-sm-10">
                              <input style="width:350px; type=" text" class="form-control" id="tenLop"
                              placeholder="Enter TenLop" name="tenLop" value="${list.tenLop}">
                         </div>
                     </div>

                   </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10" style="text-align:center;">
                                <button type="submit" onsubmit="alert('edit department success !')"
                                style="background:#ccc;border-radius:6px;border:none;width:80px;padding:7px 10px;">Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
        <!-- end modal -->

        <!-- addModal -->
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog" style="margin-left: 404px;">
                        <!-- Modal content-->
                        <div class="modal-content" style="height: 257px;">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Edit Department</h4>
                            </div>
                            <div class="modal-body">

                                <div class="container">
                                    <form class="form-horizontal" method="post" action="/save">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="tenLop">TenLop</label>
                                            <div class="col-sm-10">
                                                <input style="width:350px; type=" text" class="form-control" id="tenLop"
                                                    placeholder="Enter TenLop" name="tenLop">
                                            </div>
                                        </div>

                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10" style="text-align:center;">
                                        <button type="submit" onsubmit="alert('add department success !')";
                                            style="background:#ccc;border-radius: 6px;border: none;width: 80px;padding: 7px 10px;">Save</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

       <!-- endAddModal -->

    </div>
        <script src="webjars/jquery/1.9.1/jquery.min.js"></script>
        <script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.js" ></script>
         <script src="http://1892.yn.lt/blogger/JQuery/Pagging/js/jquery.twbsPagination.js" type="text/javascript"></script>
        <script>
            function myFunction() {
                if (confirm("bạn có chắc muốn xóa bản ghi này không ? ")) {

                } else {
                    return false;
                }
            }

            $(function () {
                   var pageSize = 5;
                    showPage = function (page) {
                       $(".contentPage").hide();
                       $(".contentPage").each(function (n) {
                        if (n >= pageSize * (page - 1) && n < pageSize * page)
                                        $(this).show();
                           });
                         }
                            showPage(1);
                            var totalRows = ${totalItems};
                            var btnPage = ${totalItems} / pageSize;
                            var iTotalPages = Math.ceil(totalRows / pageSize);

                            var obj = $('#pagination').twbsPagination({
                                totalPages: iTotalPages,
                                visiblePages: btnPage,
                                onPageClick: function (event, page) {
                                    console.info(page);
                                    showPage(page);
                                }
                            });
                            console.info(obj.data());
                        });

        </script>
</body>

</html>